package cl.music.client.service;

import cl.music.client.domain.TrackDTO;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.model_objects.specification.ArtistSimplified;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.requests.data.search.simplified.SearchTracksRequest;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class SpotifyClientTest {

    SpotifyClient spotifyClient = new SpotifyClient();

    private SpotifyApi spotifyApi;

    @Test
    void getToken() {

        String token;
        try {
            token = spotifyClient.getToken();

            spotifyApi = new SpotifyApi.Builder()
                .setAccessToken(token)
                .build();

            SearchTracksRequest searchTracksRequest = spotifyApi.searchTracks("ele").limit(50).build();


            Paging<Track> trackPaging = searchTracksRequest.execute();


            List<Track> tracks = Arrays.asList(trackPaging.getItems());


            List<TrackDTO> listt = tracks.stream().map(
                t -> {
                    TrackDTO track = new TrackDTO();
                    track.setId(t.getId());
                    track.setName(t.getName());
                    track.setArtists(convertArrayToStringMethod(t.getArtists()));
                    track.setStar(t.getPopularity());

                    return track;
                }).collect(Collectors.toList());


            System.out.println("Total: " + trackPaging.getTotal());



//
//            ResponseAlbum albums = spotifyClient.getAlbums("electronica", token);
//
//            albums.getAlbums().getItems().forEach(
//                item -> System.out.println("nombre: " + item.getName())
//            );

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static String convertArrayToStringMethod(ArtistSimplified[] strArray) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < strArray.length; i++) {
            stringBuilder.append("; " + strArray[i].getName());
        }
        return stringBuilder.toString();
    }
}
