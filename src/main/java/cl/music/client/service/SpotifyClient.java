package cl.music.client.service;

import cl.music.client.domain.ReponseTokenSpotify;
import cl.music.client.domain.TrackDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.ArtistSimplified;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.requests.data.search.simplified.SearchTracksRequest;
import io.prometheus.client.Collector;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.client.HttpClient;
import static org.apache.http.HttpHeaders.USER_AGENT;

@Service
public class SpotifyClient {

    private String clientId = "4acdf402c09947a3bb8f558e9e5dcf6d";
    private String clientSecret = "8cf349a9a1c1492591036ae95b38506f";
    private String urlToken = "https://accounts.spotify.com/api/token";

    private SpotifyApi spotifyApi;

    ObjectMapper objectMapper = new ObjectMapper();

    public String getToken() throws Exception {

        try {
            String idSecret = clientId + ":" + clientSecret;
            String idSecretEncoded = new String(Base64.encodeBase64(idSecret.getBytes()));

            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(urlToken);

            post.setHeader("User-Agent", USER_AGENT);
            post.setHeader("Content-Type", "application/x-www-form-urlencoded");
            post.setHeader("Authorization", "Basic " + idSecretEncoded);

            List<NameValuePair> urlParameters = new ArrayList<>();
            urlParameters.add(new BasicNameValuePair("grant_type", "client_credentials"));

            post.setEntity(new UrlEncodedFormEntity(urlParameters));

            HttpResponse response = null;
            response = client.execute(post);
            String jsonResponse = EntityUtils.toString(response.getEntity());

            ReponseTokenSpotify reponseTokenSpotify = objectMapper.readValue(jsonResponse, ReponseTokenSpotify.class);

            return reponseTokenSpotify.getAccessToken();

        } catch (IOException e) {
            throw new Exception("FALLO LA OPTENCION DE TOKEN: " + e.getMessage());
        }
    }

    public List<TrackDTO> getTrack(String muse, int limit, int offSet){

        try {
            spotifyApi = new SpotifyApi.Builder()
                .setAccessToken(getToken())
                .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        SearchTracksRequest searchTracksRequest = spotifyApi.searchTracks(muse!= null ? muse : "ele")
            .limit(limit)
            .offset(offSet)
            .build();

        Paging<Track> trackPaging = null;
        try {
            trackPaging = searchTracksRequest.execute();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SpotifyWebApiException e) {
            e.printStackTrace();
        }

        List<Track> tracks = Arrays.asList(trackPaging.getItems());

        return tracks.stream().map(
            t -> {
                TrackDTO track = new TrackDTO();
                track.setId(t.getId());
                track.setName(t.getName());
                track.setArtists(convertArrayToStringMethod(t.getArtists()));
                track.setStar(t.getPopularity()/10);

            return track;
        }).collect(Collectors.toList());

    }


    public static String convertArrayToStringMethod(ArtistSimplified[] strArray) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < strArray.length; i++) {
            stringBuilder.append(strArray[i].getName() + "; " );
        }
        return stringBuilder.toString();
    }

}
