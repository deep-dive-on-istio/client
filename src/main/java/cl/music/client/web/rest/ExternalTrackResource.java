package cl.music.client.web.rest;

import cl.music.client.domain.TrackDTO;
import cl.music.client.service.SpotifyClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller for managing {}.
 */
@RestController
@RequestMapping("/api")
public class ExternalTrackResource {

    private final Logger log = LoggerFactory.getLogger(ExternalTrackResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SpotifyClient spotifyClient;

    public ExternalTrackResource(SpotifyClient spotifyClient) {
        this.spotifyClient = spotifyClient;
    }

    /**
     * {@code GET  /tracks/:muse} : get the "id" track.
     *
     * @param muse the id of the trackDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the trackDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tracks")
    public ResponseEntity<List<TrackDTO>> getTrack(@RequestParam("muse") String muse,
                                                   @RequestParam("limit") int limit,
                                                   @RequestParam("offset") int offset) {
        log.debug("REST request to get a page of Tracks");

        return ResponseEntity.ok(spotifyClient.getTrack(muse, limit, offset));
    }

}
