/**
 * View Models used by Spring MVC REST controllers.
 */
package cl.music.client.web.rest.vm;
